//
//  MasterViewController.swift
//  HKTutorial
//
//  Created by ernesto on 18/10/14.
//  Copyright (c) 2014 raywenderlich. All rights reserved.
//

import Foundation
import HealthKit
import UIKit
import Alamofire

class MasterViewController: UITableViewController {
  
  let kAuthorizeHealthKitSection = 2
  let kSendLatestWeightToDTUServer = 4
  let kProfileSegueIdentifier = "profileSegue"
  let kWorkoutSegueIdentifier = "workoutsSeque"
  let kRealTimeSegueIdentifier = "realtimeSegue"
  
  let healthManager:HealthManager = HealthManager()
  
  let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate

  var weight:HKQuantitySample?
  
  func authorizeHealthKit()  {
    
    healthManager.authorizeHealthKit { (authorized,  error) -> Void in
      if authorized {
        print("HealthKit authorization received.")
      }
      else
      {
        print("HealthKit authorization denied!")
        if error != nil {
          print("\(error)")
        }
      }
    }
  }
  
  func sendLatestWeight() {
      // 1. Construct an HKSampleType for weight
    
      let sampleType = HKSampleType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyMass)
      // 2. Call the method to read the most recent weight sample
      self.healthManager.readMostRecentSample(sampleType!, completion: { (mostRecentWeight, error) -> Void in
        
        if( error != nil )
        {
          print("Error reading Weight from HealthKit Store: \(error.localizedDescription)")
          return;
        }
      
        print(mostRecentWeight)
        print("mongol")
        
      });
  }
  
  // MARK: - Segues
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    if segue.identifier ==  kProfileSegueIdentifier {
      
      if let profileViewController = segue.destinationViewController as? ProfileViewController {
        profileViewController.healthManager = healthManager
      }
    }
    else if segue.identifier == kWorkoutSegueIdentifier {
      if let workoutViewController = segue.destinationViewController as? WorkoutsTableViewController {
        workoutViewController.healthManager = healthManager;
      }
    }
    else if segue.identifier == kRealTimeSegueIdentifier {
      if let realtimeViewController = segue.destinationViewController as? RealTimeViewController {
        realtimeViewController.healthManager = healthManager;
      }
    }
  }
  
  // MARK: - TableView Delegate
  override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    
    switch (indexPath.section, indexPath.row)
    {
    case (kAuthorizeHealthKitSection,0):
      authorizeHealthKit()
    case (kSendLatestWeightToDTUServer,0):
      let sampleType = HKSampleType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyMass)
      self.healthManager.readMostRecentSample(sampleType!, completion: { (mostRecentWeight, error) -> Void in
        self.weight = mostRecentWeight as? HKQuantitySample
        //print(mostRecentWeight.description)
        let metadataweight: NSDictionary = [HKMetadataKeyWasUserEntered: mostRecentWeight.metadata!]

        print(metadataweight["HKWasUserEntered"]!)
        
        print(mostRecentWeight.endDate)
        print(self.weight!.quantity)
      })
    default:
      break
    }
    self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
  }
  
  
  
}
