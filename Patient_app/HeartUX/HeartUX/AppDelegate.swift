//
//  AppDelegate.swift
//  HeartUX
//
//  Created by Henrik Holm on 18/10/14.
//  Copyright (c) 2015 Henrik Holm. All rights reserved.
//

import UIKit
import HealthKit
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  
  var window: UIWindow?
  
  let healthManager:HealthManager = HealthManager()
    

  
  func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
      // Override point for customization after application launch.
      
      print("Lauching")
      application.registerUserNotificationSettings(UIUserNotificationSettings(forTypes: [.Alert, .Badge, .Sound], categories: nil))
    
      healthManager.authorizeHealthKit { (authorized,  error) -> Void in
        if authorized {
          print("HealthKit authorization received.")
        }
        else {
          print("HealthKit authorization denied!")
          if error != nil {
            print("\(error)")
          }
        }
      }
    
      Fabric.with([Crashlytics.self])
    
      // TODO: Move this to where you establish a user session
      self.logUser()

    
      return true

    }
  
    func logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        Crashlytics.sharedInstance().setUserEmail("user@fabric.io")
        Crashlytics.sharedInstance().setUserIdentifier("12345")
        Crashlytics.sharedInstance().setUserName("Test User")
    }

  func application(application: UIApplication, didReceiveLocalNotification notification: UILocalNotification) {

//    let storyboard = UIStoryboard(name: "Main", bundle: nil)
//    let vc = storyboard.instantiateViewControllerWithIdentifier("RealTimeViewController")
//    let navigationController = UINavigationController(rootViewController: vc)
//    self.window?.rootViewController?.presentViewController(navigationController, animated: true, completion: nil)
    
  }

  func applicationWillResignActive(application: UIApplication) {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
  }
  
  func applicationDidEnterBackground(application: UIApplication) {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
  }
  
  func applicationWillEnterForeground(application: UIApplication) {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
  }
  
  func applicationDidBecomeActive(application: UIApplication) {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
  }
  
  func applicationWillTerminate(application: UIApplication) {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
  }
  
  
}

