//
//  LocalStorage.swift
//  HeartUX
//
//  Created by Henrik Holm on 01/01/2016.
//  Copyright © 2016 Henrik Holm. All rights reserved.
//

import UIKit
import HealthKit

struct LocalStore {
    
    static func getAnchor(anchorkey: String) -> HKQueryAnchor? {
        let encoded = NSUserDefaults.standardUserDefaults().dataForKey(anchorkey)
        if(encoded == nil){
            return nil
        }
        let anchor = NSKeyedUnarchiver.unarchiveObjectWithData(encoded!) as? HKQueryAnchor
        return anchor
    }

    static func saveAnchor(anchor : HKQueryAnchor, anchorkey: String) {
        let encoded = NSKeyedArchiver.archivedDataWithRootObject(anchor)
        NSUserDefaults.standardUserDefaults().setValue(encoded, forKey: anchorkey)
        NSUserDefaults.standardUserDefaults().synchronize()
    }

    static func saveAuthKey(auth: String) {
        NSUserDefaults.standardUserDefaults().setObject(auth, forKey: "authKey")
    }
    
    static func saveUsernameForUser(email: String) {
        NSUserDefaults.standardUserDefaults().setObject(String(email), forKey: "username")
    }
    
    static func getUsernameForUser() -> String {
        let username = NSUserDefaults.standardUserDefaults().objectForKey("username")! as! String
    return username
        
    }
    
    static func getAuthKey() -> String {
        let auth = NSUserDefaults.standardUserDefaults().objectForKey("authKey")! as! String
    return auth
    }
    
}
    