//
//  WeightTableViewController.swift
//  HeartUX
//
//  Created by Henrik Holm on 20/12/2015.
//  Copyright © 2015 Henrik Holm. All rights reserved.
//

import Foundation
import HealthKit
import UIKit
import Alamofire
import SwiftyJSON

class WeightTableViewController: UITableViewController {
    
    let healthManager:HealthManager = HealthManager()
    let dtuServer:DTUServer = DTUServer()
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    let username = NSUserDefaults.standardUserDefaults().objectForKey("username")! as! String
    let auth = NSUserDefaults.standardUserDefaults().objectForKey("authKey")! as! String

    var weights = [Weight]()
    
//    func getWeights() {
//    
//        dtuServer.getUserWeightsFromDTUServer(username, auth: auth) { (json, error) -> Void in
//    
//            if error != nil {
//                print(error)
//            } else {
//                self.weights = []
//                var weightArray: [JSON] = []
//                weightArray = json["_items"].arrayValue
//                print(weightArray.count)
//    
//                for wa in weightArray {
//                            
//                    let weight = Weight()
//                    let measurement = wa["measurement"].floatValue
//                    //let measurementFormatted = String(format: "%.2f", measurement) + " kg"
//                    let date = wa["date"].stringValue
//                            
//                    weight.measurement = measurement
//                    weight.date = date
//                            
//                    self.weights.append(weight)
//
//                }
//                self.tableView.reloadData()
//
//            }
//        }
//
//    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
//        getWeights()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

        override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
            return weights.count
    
        }
    
        override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    
            let cell = UITableViewCell(style: UITableViewCellStyle.Value1, reuseIdentifier: "Cell")
    
            cell.textLabel?.text = String(weights[indexPath.row].measurement) + " kg"
            cell.detailTextLabel?.text = weights[indexPath.row].date
            
            return cell
        }


    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
