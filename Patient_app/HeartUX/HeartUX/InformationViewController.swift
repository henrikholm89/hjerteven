//
//  InformationViewController.swift
//  HeartUX
//
//  Created by Henrik Holm on 17/12/2015.
//  Copyright © 2015 Henrik Holm. All rights reserved.
//

import UIKit

class InformationViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIWebViewDelegate {

  
    @IBOutlet var tableView: UITableView!

    let youtubeVidoes: [String] = ["http://www.youtube.com/embed/g-C7uY818b0", "http://www.youtube.com/embed/Ail7av7QbB8"]
    
    var contentHeights : [CGFloat] = [0.0, 0.0]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getYouTubeVideos(youTubeVidoes: [String]) {
        
        for yuv in youTubeVidoes {
            
            let youtubeLink: String = "http://www.youtube.com/embed/" + yuv
            let width = 300
            let height = 200
            let frame = 50
            
            let code: NSString = "<iframe width=\(width) height=\(height) src=\(youtubeLink) frameborder=\(frame) allowfullscreen></iframe>";
            
//            self.webView.loadHTMLString(code as String, baseURL: nil)
            
        }
        
        
    }
    private func facetime(phoneNumber:String) {
        if let facetimeURL:NSURL = NSURL(string: "facetime://\(phoneNumber)") {
            let application:UIApplication = UIApplication.sharedApplication()
            if (application.canOpenURL(facetimeURL)) {
                application.openURL(facetimeURL);
            }
        }
    }
    
    @IBAction func contactDoctor(sender: AnyObject) {
        facetime("appleid@zandi.dk")
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return youtubeVidoes.count
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("youtube", forIndexPath: indexPath) as! VideosTableViewCell
        let youtubeLink = youtubeVidoes[indexPath.row]
        let htmlHeight = contentHeights[indexPath.row]
        
        
        cell.webView.tag = indexPath.row
        cell.webView.delegate = self
        
        let width = 300
        let height = 200
        let frame = 50
        
        let code: NSString = "<iframe width=\(width) height=\(height) src=\(youtubeLink) frameborder=\(frame) allowfullscreen></iframe>";
        
        cell.webView.loadHTMLString(code as String, baseURL: nil)
        cell.webView.frame = CGRectMake(30, 0, cell.frame.size.width, htmlHeight)
//
//        cell.webView.loadHTMLString(htmlString, baseURL: nil)
//        cell.webView.frame = CGRectMake(0, 0, cell.frame.size.width, htmlHeight)
        
        return cell
    }
    

    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return contentHeights[indexPath.row]
    }

    func webViewDidFinishLoad(webView: UIWebView)
    {
        if (contentHeights[webView.tag] != 0.0)
        {
            // we already know height, no need to reload cell
            return
        }
        
        contentHeights[webView.tag] = webView.scrollView.contentSize.height
        tableView.reloadRowsAtIndexPaths([NSIndexPath(forRow: webView.tag, inSection: 0)], withRowAnimation: .Automatic)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}



