//
//  SignUpViewController.swift
//  HeartUX
//
//  Created by Henrik Holm on 18/11/2015.
//  Copyright © 2015 Henrik Holm. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController, UITextFieldDelegate {
    
    let dtuServer:DTUServer = DTUServer()
    let healthManager:HealthManager = HealthManager()

    
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var firstNameTextField: UITextField!
    @IBOutlet var lastNameTextField: UITextField!
    
    let password: String = "test1234"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.emailTextField.delegate = self
        self.firstNameTextField.delegate = self
        self.lastNameTextField.delegate = self
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func signUpButton(sender: AnyObject) {
    
        let email = emailTextField.text
        let firstName = firstNameTextField.text
        let lastName = lastNameTextField.text

        if email!.isEmpty || firstName!.isEmpty || lastName!.isEmpty {
            
            displayMyAlertMessage("All fields are required")
            return
            
        }
        
        // Post to DTU Server
        
        dtuServer.registerNewUser(email!, firstname: firstName!, lastname: lastName!, completion: { (response, error) -> Void in
            if error != nil {
                
                self.displayMyAlertMessage("User Not created at backend - try again")
                
            } else {
                
                print(response)
                if response["_status"] == "OK" {
                    
                    let auth = HelperFunctions.getBasicAuthHeaders(email!, password: "test1234")

//                    NSUserDefaults.standardUserDefaults().setObject(String(auth), forKey: "authKey")
//                    NSUserDefaults.standardUserDefaults().setObject(String(email!), forKey: "username")

                    LocalStore.saveAuthKey(String(auth))
                    LocalStore.saveUsernameForUser(String(email!))

                    self.healthManager.authorizeHealthKit({ (success, error) -> Void in
                        if success {
                            print("Starting sending previous collected data to backend")
                        }
                        else {
                            print("HealthKit authorization denied!")
                            if error != nil {
                                print("\(error)")
                            }
                        }

                    })
                    
                    self.performSegueWithIdentifier("homeScreen", sender: self)

                }
        
                else {
                    
                    self.displayMyAlertMessage("User already exist - try a different username")

                }

                
            }
        })
        
    }
    
//    @IBAction func loginButton(sender: AnyObject) {
//        
//        let username = usernameField.text
//        let password = passwordField.text
//        
//        
//        if username!.isEmpty || password!.isEmpty {
//            
//            displayMyAlertMessage("All fields are required")
//            return
//            
//        }
//        
//        // GET to DTU Server
//        
//        dtuServer.loginUser(username!, password: password!, completion: { (response, error) -> Void in
//            if error != nil {
//                
//                self.displayMyAlertMessage("Failed while logging in, try again")
//                
//            } else {
//                
//                print(response)
//                
//                if response["_status"] == "ERR" {
//                    
//                    self.displayMyAlertMessage("Failed while logging in, try again")
//
//                } else {
//                    
//                    NSUserDefaults.standardUserDefaults().setObject(username, forKey: "username")
//                    NSUserDefaults.standardUserDefaults().setObject(password, forKey: "password")
//                    
//                    self.performSegueWithIdentifier("homeScreen", sender: self)
//                    //self.displayMyAlertMessage("Logged in succesfully :)")
//                    
//                }
//                
//            }
//        })
//
//    }
    
    func displayMyAlertMessage(userMessage:String) {
        
        let myAlert = UIAlertController(title:"Alert", message:userMessage, preferredStyle: UIAlertControllerStyle.Alert);
        
        let okAction = UIAlertAction(title:"Ok", style:UIAlertActionStyle.Default, handler:nil);
        
        myAlert.addAction(okAction);
        
        self.presentViewController(myAlert, animated:true, completion:nil);
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        let prefs:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        
        let username = prefs.objectForKey("username")
        let password = prefs.objectForKey("authKey")
        
        print(username, password)
        
        if (username != nil && password != nil) {
            print("logged in")
            self.performSegueWithIdentifier("homeScreen", sender: self)
        }
    }
        
    override func viewWillAppear(animated: Bool) {
        let prefs:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        
        let username = prefs.objectForKey("username")
        let password = prefs.objectForKey("authKey")
        
        if (username != nil && password != nil) {
            self.view.hidden = true
        } else {
            self.view.hidden = false
        }

    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true);
        return false;
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        self.view.endEditing(true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
