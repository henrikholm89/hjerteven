//
//  RealTimeViewController.swift
//  HKTutorial
//
//  Created by Henrik Holm on 29/09/15.
//  Copyright © 2015 raywenderlich. All rights reserved.
//

import UIKit
import HealthKit

class RealTimeViewController: UITableViewController {
    
  
    var healthManager:HealthManager?

    @IBOutlet var bloodpressureLabel: UILabel!
    @IBOutlet var pulsLabel: UILabel!
    @IBOutlet var oxygensaturationLabel: UILabel!
    @IBOutlet var bodyfatLabel: UILabel!
    @IBOutlet var weightLabel: UILabel!
    
    var oxygenSaturation:HKQuantitySample?
    var weight:HKQuantitySample?
    var bodyfat:HKQuantitySample?
    
    let kUnknownString   = "Unknown"

    
//    func updateOxygenSaturation() {
//        let sampleType = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierOxygenSaturation)
//        
//        self.healthManager?.readMostRecentSample(sampleType!, completion: { (mostRecentOxygenSaturation, error) -> Void in
//            if error != nil {
//                print("Error reading Oxygen Saturation from HealthKit Store: \(error.localizedDescription)")
//                return;
//            }
//   
//            var oxygenSaturationLocalizedString = self.kUnknownString;
//            self.oxygenSaturation = mostRecentOxygenSaturation as? HKQuantitySample
//            // 3. Format the height to display it on the screen
//            if let percentDobule = self.oxygenSaturation?.quantity.doubleValueForUnit(HKUnit.percentUnit()) {
//                
//                let percent = percentDobule*100.0
//                oxygenSaturationLocalizedString = NSString(format: "%.0f%%", percent) as String
//
//            }
//            
//            dispatch_async(dispatch_get_main_queue(), { () -> Void in
//                self.oxygensaturationLabel.text = oxygenSaturationLocalizedString
//            });
//            
//        })
//    }
//    
//    func updateWeight() {
//        // 1. Construct an HKSampleType for weight
//        let sampleType = HKSampleType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyMass)
//        
//        // 2. Call the method to read the most recent weight sample
//        self.healthManager?.readMostRecentSample(sampleType!, completion: { (mostRecentWeight, error) -> Void in
//            
//            if( error != nil )
//            {
//                print("Error reading Weight from HealthKit Store: \(error.localizedDescription)")
//                return;
//            }
//            
//            var weightLocalizedString = self.kUnknownString;
//            // 3. Format the weight to display it on the screen
//            self.weight = mostRecentWeight as? HKQuantitySample;
//            if let kilograms = self.weight?.quantity.doubleValueForUnit(HKUnit.gramUnitWithMetricPrefix(.Kilo)) {
//                let weightFormatter = NSMassFormatter()
//                weightFormatter.forPersonMassUse = true;
//                weightLocalizedString = weightFormatter.stringFromKilograms(kilograms)
//                print(weightLocalizedString)
//                print(kilograms)
//            }
//            
//            // 4. Update UI in the main thread
//            dispatch_async(dispatch_get_main_queue(), { () -> Void in
//                self.weightLabel.text = weightLocalizedString
//            });
//        });
//        
//    }
//    
//    func updateBodyFat() {
//        let sampleType = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyFatPercentage)
//        
//        self.healthManager?.readMostRecentSample(sampleType!, completion: { (mostRecentBodyFat, error) -> Void in
//            if error != nil {
//                print("Error reading Body Fat from HealthKit Store: \(error.localizedDescription)")
//                return;
//            }
//            
//            var bodyFatLocalizedString = self.kUnknownString;
//            self.bodyfat = mostRecentBodyFat as? HKQuantitySample
//            // 3. Format the height to display it on the screen
//
//            if let percentDobule = self.bodyfat?.quantity.doubleValueForUnit(HKUnit.percentUnit()) {
//                let percent = percentDobule*100.0
//                bodyFatLocalizedString = NSString(format: "%.2f%%", percent) as String
//                
//            }
//            
//            dispatch_async(dispatch_get_main_queue(), { () -> Void in
//                self.bodyfatLabel.text = bodyFatLocalizedString
//            });
//            
//        })
//    }

  
    override func viewDidLoad() {
        super.viewDidLoad()
//        updateBodyFat()
//        updateOxygenSaturation()
//        updateWeight()
        

      // Uncomment the following line to preserve selection between presentations
      // self.clearsSelectionOnViewWillAppear = false

      // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
      // self.navigationItem.rightBarButtonItem = self.editButtonItem()
  }


  override func didReceiveMemoryWarning() {
      super.didReceiveMemoryWarning()
      // Dispose of any resources that can be recreated.
  }
    

    // MARK: - Table view data source
/*
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 0
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 0
    }

  
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
