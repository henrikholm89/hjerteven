//
//  DTUServer.swift
//  HeartUX
//
//  Created by Henrik Holm on 29/10/2015.
//  Copyright © 2015 Henrik Holm. All rights reserved.
//

import Foundation
import HealthKit
import UIKit
import Alamofire
import SwiftyJSON

class DTUServer {

    
//    private enum ResourcePath: CustomStringConvertible {
//        case signUp
//        case Login
//        case userWeightsGrouped(pastDateFormatted: String, email: String)
//        case userWeights(username: String)
//        
//        var description: String {
//            switch self {
//            case .signUp: return "/user"
//            case .Login: return "/user"
//            case .userWeightsGrouped(let pastDateFormatted, let email): return "weight?where={\"date\": {\"$gt\": \"\(pastDateFormatted)\"}, \"username\": \"\(email)\"}&sort=-date"
//            case .userWeights(let id): return "/api/v1/stories/\(id)/reply"
//            }
//        }
//    }

    
    let healthManager:HealthManager = HealthManager()
    //var weight:HKQuantitySample?
    var weightsGroupedByDate = [Weight]()
    var mostRecentMeasurement: Float = 0.0
    var bloodPressures = [BloodPressure]()
    
    let baseURL: String = "http://heartux.compute.dtu.dk:443"
    
    // Requests
    
    func getUserWeightsGroupedFromDTUServer(username: String, auth: String, last3days: Bool, completion: (([Weight], Float, NSError!) -> Void)!) {
        
        let pastDate = NSDate(timeIntervalSinceNow: -3*24*60*60)
        let pastDateFormatted = HelperFunctions.convertDateToString(pastDate)
        
        print(pastDateFormatted)
        var url: String
        
        if last3days == true {
            url = baseURL + "/weight?where={\"date\": {\"$gt\": \"\(pastDateFormatted)\"}, \"username\": \"\(username)\"}&sort=-date"
        } else {
            url = baseURL + "/weight?where={\"username\": \"\(username)\"}&sort=-date"
        }
        
        let request = url.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!

        let headers = [
            "Access-Control-Request-Headers": "accept, origin, authorization",
            "Access-Control-Expose-Headers": "Origin, X-Requested-With, Content-Type, Accept",
            "Cache-Control": "no-cache",
            "Authorization": "Basic " + auth
            
        ]
        
        Alamofire.request(.GET, request, headers: headers)
            .response {(request, response, data, error) in
                
                if error != nil {
                    completion(self.weightsGroupedByDate,self.mostRecentMeasurement,error)
                } else {
                    self.weightsGroupedByDate = []
                    let json = JSON(data: data!)
                    
                    guard let weightArray: [JSON] = json["_items"].arrayValue else {
                        return
                    }
                    
                    var sum: Float = 0
                    var count: Int = 0
                    
                    // Group measurements by date by taken the mean - there can be multiple measurements each day.
                    // It is very messy and not very clean. This should be handle at server side imo - just a group by in SQL
                    
                    for var i=0; i<weightArray.count; i++ {
                        
                        if weightArray.count > 1 {
                            
                            self.mostRecentMeasurement = weightArray[0]["measurement"].floatValue
                            let weightDataGrouped = Weight()
                            
                            let weight = weightArray[i]["measurement"].floatValue
                            let date = weightArray[i]["date"].stringValue
                            let dateFormatted = date.substringWithRange(0, end: 8)
                            
                            // Avoids Array out of bounce
                            if i == weightArray.count-1 {
                                let previousDate = weightArray[i-1]["date"].stringValue
                                let previousDateFormatted = previousDate.substringWithRange(0, end: 8)
                                
                                if dateFormatted == previousDateFormatted {
                                    sum += weightArray[i]["measurement"].floatValue
                                    count += 1
                                    let mean = sum/Float(count)
                                    weightDataGrouped.measurement = mean
                                } else {
                                    if count > 0 {
                                        sum += weightArray[i]["measurement"].floatValue
                                        count += 1
                                        let mean = sum/Float(count)
                                        weightDataGrouped.measurement = mean
                                    } else {
                                        weightDataGrouped.measurement = weight
                                    }
                                }
                                weightDataGrouped.date = dateFormatted
                                
                                self.weightsGroupedByDate.append(weightDataGrouped)
                                
                                break
                                
                            }
                            
                            let nextDate = weightArray[i+1]["date"].stringValue
                            let nextDateFormatted = nextDate.substringWithRange(0, end: 8)
                            
                            if dateFormatted == nextDateFormatted {
                                sum += weightArray[i]["measurement"].floatValue
                                count += 1
                            } else {
                                if count > 0 {
                                    sum += weightArray[i]["measurement"].floatValue
                                    count += 1
                                    let mean = sum/Float(count)
                                    weightDataGrouped.measurement = mean
                                    print(mean)
                                    sum = 0
                                    count = 0
                                } else {
                                    weightDataGrouped.measurement = weight
                                }
                                
                                weightDataGrouped.date = dateFormatted
                                
                                self.weightsGroupedByDate.append(weightDataGrouped)
                            }
                            
                        } else if weightArray.count > 0 {
                            self.mostRecentMeasurement = weightArray[0]["measurement"].floatValue
                            let weightDataGrouped = Weight()
                            let weight = weightArray[i]["measurement"].floatValue
                            let date = weightArray[i]["date"].stringValue
                            let dateFormatted = date.substringWithRange(0, end: 8)
                            
                            weightDataGrouped.measurement = weight
                            weightDataGrouped.date = dateFormatted
                            
                            self.weightsGroupedByDate.append(weightDataGrouped)
                        }

                    }

                    completion(self.weightsGroupedByDate,self.mostRecentMeasurement,nil)
                }

        }
        
    }
    
    
//    func getUserWeightsFromDTUServer(username: String, auth: String, completion: ((JSON, NSError!) -> Void)!) {
//                
//        let url = baseURL + "/weight?where={\"username\": \"\(username)\"}&sort=-_created"
//        let request = url.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
//        print(request)
//        
//        let headers = [
//            "Access-Control-Request-Headers": "accept, origin, authorization",
//            "Access-Control-Expose-Headers": "Origin, X-Requested-With, Content-Type, Accept",
//            "Cache-Control": "no-cache",
//            "Authorization": "Basic " + auth
//            
//        ]
//        
//        Alamofire.request(.GET, request, headers: headers)
//            .response {(request, response, data, error) in
//                
//                if error != nil {
//                    completion(nil,error)
//                } else {
//                    let json = JSON(data: data!)
//                    completion(json,nil)
//                }
//                
//        }
//        
//    }

    
    func getUserBodyFatFromDTUServer(username: String, auth: String, completion: ((JSON, NSError!) -> Void)!) {
        
        let request = baseURL + "/bodyfat?where=username==\(username)&sort=-date"
        
        let headers = [
            "Access-Control-Request-Headers": "accept, origin, authorization",
            "Access-Control-Expose-Headers": "Origin, X-Requested-With, Content-Type, Accept",
            "Cache-Control": "no-cache",
            "Authorization": "Basic " + auth
        ]
    
        Alamofire.request(.GET, request, headers: headers)
            .response {(request, response, data, error) in
                
                if error != nil {
                    completion(nil,error)
                } else {
                    let json = JSON(data: data!)
                    
                    completion(json,nil)
                }
        }
        
    }
    
    func getUserBloodPressureFromDTUServer(username: String, auth: String, completion: (([BloodPressure], NSError!) -> Void)!) {
        
        let url = baseURL + "/bloodpressure?where={\"username\": \"\(username)\"}&sort=-date"

        let request = url.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!
    
        let headers = [
            "Access-Control-Request-Headers": "accept, origin, authorization",
            "Access-Control-Expose-Headers": "Origin, X-Requested-With, Content-Type, Accept",
            "Cache-Control": "no-cache",
            "Authorization": "Basic " + auth
        ]

        
        Alamofire.request(.GET, request, headers: headers)
            .response {(request, response, data, error) in
                
                if error != nil {
                    completion(self.bloodPressures,error)
                    
                } else {
                    
                    let json = JSON(data: data!)
                    print(json)

                    self.bloodPressures = []
                    
                    guard let bloodPressureArray: [JSON] = json["_items"].arrayValue else {
                        return
                    }
                
                    for bpa in bloodPressureArray {
                            
                        let bloodpressure = BloodPressure()
                        
                        let systolic = bpa["systolic"].floatValue
                        let systolicFormatted = String(format: "%.0f", systolic)
                        
                        let diastolic = bpa["diastolic"].floatValue
                        let diastolicFormatted = String(format: "%.0f", diastolic)
        
                        let bloodpressureCombined: String = systolicFormatted + "/" + diastolicFormatted + " mm Hg"
                            
                        let date = bpa["date"].stringValue
                        
                        let source = bpa["source"].stringValue
                            
                        bloodpressure.systolic = systolic
                        bloodpressure.diastolic = diastolic
                        bloodpressure.bloodPressureCombined = bloodpressureCombined
                        bloodpressure.date = date
                        bloodpressure.sourceType = source
                            
                        self.bloodPressures.append(bloodpressure)
                            
                        }
        
                    completion(self.bloodPressures,nil)
                }
                
        }
        
    }

    
    func registerNewUser(email: String, firstname: String, lastname: String, completion: ((JSON, NSError!) -> Void)!) {
        
        let username = "admin"
        let password = "kummefryser"
        
        let request = baseURL + "/user"
        let auth = HelperFunctions.getBasicAuthHeaders(username, password: password)
        
        let headers = [
            "Authorization": "Basic " + auth
        ]
        
        let parameters = [
            "username": email,
            "firstname": firstname,
            "lastname": lastname,
            "password": "test1234"
        ]
        
        Alamofire.request(.POST, request, headers: headers, parameters: parameters)
            .response {(request, response, data, error) in
                
                if error != nil {
                    completion(nil, error)
                } else {
                    let json = JSON(data: data!)
                    //print(response)
                    completion(json, nil)
                    
                }

        }
        
    }
    
    func loginUser(username: String, password: String, completion: ((JSON, NSError!) -> Void)!) {
     
        let base64String = HelperFunctions.getBasicAuthHeaders(username, password: password)
        
        let request = baseURL + "/user"

        let headers = [
            "Authorization": "Basic " + base64String
        ]
        
        Alamofire.request(.GET, request, headers: headers)
            .response {(request, response, data, error) in
                
                if error != nil {
                    completion(nil, error)
                } else {
                    let json = JSON(data: data!)
                    completion(json, nil)
                    
                }
            
              
        }
        

        
        
    }
}
