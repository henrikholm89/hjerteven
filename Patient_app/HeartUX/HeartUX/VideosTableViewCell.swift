//
//  VideosTableViewCell.swift
//  HeartUX
//
//  Created by Henrik Holm on 17/12/2015.
//  Copyright © 2015 Henrik Holm. All rights reserved.
//

import UIKit

class VideosTableViewCell: UITableViewCell {

    @IBOutlet var videoLabelText: UILabel!
    var webView = UIWebView()

    override func awakeFromNib() {
        super.awakeFromNib()
        webView.frame = CGRectMake(0, 0, 300, 200)
        contentView.addSubview(webView)
    
        // Initialization code
    }

//    override func setSelected(selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//
//        // Configure the view for the selected state
//    }
    
}
