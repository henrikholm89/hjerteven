//
//  HealthManager.swift
//  HeartUX
//
//  Created by Henrik Holm on 18/10/15.
//  Copyright (c) 2015 Henrik Holm. All rights reserved.
//

import Foundation
import HealthKit
import UIKit
import Alamofire
import SwiftyJSON

class HealthManager {
  
    let healthKitStore:HKHealthStore = HKHealthStore()
    var username: String = ""
    var auth: String = ""
    
    // Authorize HealthKit //
    func authorizeHealthKit(completion: ((success:Bool, error:NSError!) -> Void)!) {
    // 1. Set the types you want to read from HK Store
        let healthKitTypesToRead = Set(
          arrayLiteral:
          HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyMass)!,
          HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeartRate)!,
          HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierStepCount)!,
          HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyFatPercentage)!,
          HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBloodPressureDiastolic)!,
          HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBloodPressureSystolic)!
        )

        // 2. Set the types you want to write to HK Store
//        let healthKitTypesToWrite = Set(
//          arrayLiteral: HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyMassIndex)!,
//          HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierActiveEnergyBurned)!,
//          HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDistanceWalkingRunning)!,
//          HKQuantityType.workoutType()
//        )

        // 3. If the store is not available (for instance, iPad) return an error and don't go on.
        if !HKHealthStore.isHealthDataAvailable()
        {
          let error = NSError(domain: "heartux.compute.dtu.dk", code: 2, userInfo: [NSLocalizedDescriptionKey:"HealthKit is not available in this Device"])
          if( completion != nil )
          {
            completion(success:false, error:error)
          }
          return;
        }

        // 4.  Request HealthKit authorization
        healthKitStore.requestAuthorizationToShareTypes(nil, readTypes: healthKitTypesToRead) { (success, error) -> Void in
          
          let sampleTypeWeight = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyMass)
          let sampleTypeBodyFat = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyFatPercentage)
          let sampleTypeHeartRate = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeartRate)
          let sampleTypeBloodPressure = HKQuantityType.correlationTypeForIdentifier(HKCorrelationTypeIdentifierBloodPressure)


          if( completion != nil )
          {
            
            // Initiate observe query to observe for changes of sampletypes in HealthKit
            
            self.startObservingChanges(sampleTypeWeight, typeOfSample: "weight")
            self.startObservingChanges(sampleTypeBodyFat, typeOfSample: "bodyfat")
            self.startObservingChanges(sampleTypeBloodPressure, typeOfSample: "bloodpressure")
//            self.startObservingChanges(sampleTypeHeartRate, typeOfSample: "heartrate")
            
            completion(success:success,error:error)
          }
        }
    }

    func startObservingChanges(sampleType: HKSampleType?, typeOfSample: String) {

        let query = HKObserverQuery(sampleType: sampleType!, predicate: nil) {
          query, completionHandler, error in
          
          if error != nil {
            print(error)
          } else {
            if typeOfSample == "weight" {
                self.weightChangedHandler(query, completionHandler: completionHandler, error: error)
            } else if typeOfSample == "bodyfat" {
                self.bodyFatChangedHandler(query, completionHandler: completionHandler, error: error)
            } else if typeOfSample == "heartrate" {
                self.heartRateChangedHandler(query, completionHandler: completionHandler, error: error)
            } else if typeOfSample == "bloodpressure" {
                self.bloodPressureChangedHandler(query, completionHandler: completionHandler, error: error)
            }
            
          }
        }

        healthKitStore.executeQuery(query)
        healthKitStore.enableBackgroundDeliveryForType(sampleType!, frequency: .Immediate, withCompletion: {(succeeded, error) in
          
          if succeeded{
            print("Enabled background delivery of \(typeOfSample) changes")
          } else {
            if let theError = error{
              print("Failed to enable background delivery of \(typeOfSample) changes. ")
              print("Error = \(theError)")
            }
          }
        })
    }
    func bloodPressureChangedHandler(query: HKObserverQuery!, completionHandler: HKObserverQueryCompletionHandler!, error: NSError!) {
        
        let bloodPressureAnchor = "bloodpressureAnchor"
        
        let sampleType = HKQuantityType.correlationTypeForIdentifier(HKCorrelationTypeIdentifierBloodPressure)

        
        checkForNewSamplesBloodPressure(sampleType!, anchor: LocalStore.getAnchor(bloodPressureAnchor)) { (results, newAnchor, error, anchorValue) -> Void in
            if( error != nil )
            {
                print("Error reading blood pressure from HealthKit Store: \(error.localizedDescription)")
                return;
            }
            
            // Send if new data is in healthKit to DTU server and show notificaton
            if(newAnchor != nil && newAnchor != anchorValue) {
                
                if NSUserDefaults.standardUserDefaults().objectForKey("username") != nil && NSUserDefaults.standardUserDefaults().objectForKey("authKey") != nil {
                    self.username = LocalStore.getUsernameForUser()
                    self.auth = LocalStore.getAuthKey()
                } else {
                    completionHandler()
                    return
                }
                
                LocalStore.saveAnchor(newAnchor!, anchorkey: bloodPressureAnchor)

                let headers = [
                    "Authorization": "Basic " + self.auth
                ]
                
                guard let _ = HKQuantityType.correlationTypeForIdentifier(HKCorrelationTypeIdentifierBloodPressure),
                    let systolicType = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBloodPressureSystolic),
                    let diastolicType = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBloodPressureDiastolic) else {
                        // display error, etc...
                        return
                }
                
                for data in results {
                    
                        if let data1 = data.objectsForType(systolicType).first as? HKQuantitySample,
                        let data2 = data.objectsForType(diastolicType).first as? HKQuantitySample {
                            
                            let value1 = data1.quantity.doubleValueForUnit(HKUnit.millimeterOfMercuryUnit())
                            let value2 = data2.quantity.doubleValueForUnit(HKUnit.millimeterOfMercuryUnit())
                            
                            print("\(value1) / \(value2)")

                            let source = data1.sourceRevision.source.name

                            let systolic = Float(value1)
                            let diastolic = Float(value2)
                            
                            let date = data1.endDate as NSDate
                            let dateString = HelperFunctions.convertDateToString(date)
                            
                            print(self.username)
                            print(self.auth)
                            
                            let parameters: [String:AnyObject] = [
                                "systolic": systolic,
                                "diastolic": diastolic,
                                "date": dateString,
                                "username": self.username,
                                "source": source
                            ]
                            
                            // Post request to DTU Server
                            Alamofire.request(.POST, "http://heartux.compute.dtu.dk:443/bloodpressure", headers: headers, parameters: parameters)
                                .response {(request, response, data, error) in
                                    if error != nil {
                                        print(error)
                                        completionHandler()

                                    } else {
                                        
                                        let response = JSON(data: data!)
                                        print(response)
                                        
                                        if response["_status"] == "OK" {
                                            
                                            // Send the notification to the user
                                            let notification = UILocalNotification()
                                            notification.alertBody = "New blood pressure measurement (\(systolic)/\(diastolic)) succusfully sent to server"
                                            notification.alertAction = "Open"
                                            notification.soundName = UILocalNotificationDefaultSoundName
                                            UIApplication.sharedApplication().scheduleLocalNotification(notification)
                                            print(response)
                                            
                                        } else {
                                            
                                            // Send the notification to the user
                                            print(response)
                                            let notification = UILocalNotification()
                                            notification.alertBody = "Failed to sent weight measurement to server - try again!"
                                            notification.alertAction = "Open"
                                            notification.soundName = UILocalNotificationDefaultSoundName
                                            UIApplication.sharedApplication().scheduleLocalNotification(notification)
                                            
                                        }

                                        completionHandler()

                                    }
                            }
                            
                    }
                    
                }
//            completionHandler()
            }
        }
        
    }
    
    func bodyFatChangedHandler(query: HKObserverQuery!, completionHandler: HKObserverQueryCompletionHandler!, error: NSError!) {
        
        // Here you need to call a function to query the weight change
        let bodyFatAnchor = "bodyfatAnchor"
        
        let sampleType = HKSampleType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyFatPercentage)
        
        checkForNewSamples(sampleType!, anchor: LocalStore.getAnchor(bodyFatAnchor)) { (results, newAnchor, error, anchorValue) -> Void in
            if( error != nil )
            {
                print("Error reading height from HealthKit Store: \(error.localizedDescription)")
                return;
            }
            
            // Send if new data is in healthKit to DTU server and show notificaton
            if(newAnchor != nil && newAnchor != anchorValue) {
                
                if NSUserDefaults.standardUserDefaults().objectForKey("username") != nil && NSUserDefaults.standardUserDefaults().objectForKey("authKey") != nil {
                    self.username = LocalStore.getUsernameForUser()
                    self.auth = LocalStore.getAuthKey()
                } else {
                    completionHandler()
                    return
                }

                LocalStore.saveAnchor(newAnchor!, anchorkey: bodyFatAnchor)

                let headers = [
                    "Authorization": "Basic " + self.auth
                ]
                
                for data in results {
                    
                    let bodyFat = data as? HKQuantitySample
                    
                    //let date = String(mostRecentBodyFat.endDate)
                    let measurement = Float(bodyFat!.quantity.doubleValueForUnit(HKUnit.percentUnit())*100)
                    
                    let date = data.endDate as NSDate
                    let dateString = HelperFunctions.convertDateToString(date)
                    
                    let source = data.sourceRevision.source.name
            
                    print(measurement)
                    
                    let parameters: [String:AnyObject] = [
                        "measurement": measurement,
                        "date": dateString,
                        "username": self.username,
                        "source": source
                    ]
                    
                    // Post request to DTU Server
                    Alamofire.request(.POST, "http://heartux.compute.dtu.dk:443/bodyfat", headers: headers, parameters: parameters)
                        .response {(request, response, data, error) in
                            if error != nil {
                                print(error)
                                completionHandler()
                            } else {
                                let response = JSON(data: data!)
                                print(response)
                                completionHandler()

                            }
                    }
                }
             
//            completionHandler()
                
            }
            
        }
        
    }

    func heartRateChangedHandler(query: HKObserverQuery!, completionHandler: HKObserverQueryCompletionHandler!, error: NSError!) {
        
        // Here you need to call a function to query the weight change
        let heartRateAnchor = "heartrateAnchor"
        
        let sampleType = HKSampleType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeartRate)
        
        checkForNewSamples(sampleType!, anchor: LocalStore.getAnchor(heartRateAnchor)) { (results, newAnchor, error, anchorValue) -> Void in
            if( error != nil )
            {
                print("Error reading height from HealthKit Store: \(error.localizedDescription)")
                return;
            }
            
            // Send if new data is in healthKit to DTU server and show notificaton
            if(newAnchor != nil && newAnchor != anchorValue) {
                
                if NSUserDefaults.standardUserDefaults().objectForKey("username") != nil && NSUserDefaults.standardUserDefaults().objectForKey("authKey") != nil {
                    self.username = LocalStore.getUsernameForUser()
                    self.auth = LocalStore.getAuthKey()
                    
                } else {
                    completionHandler()
                    return
                }
                
                LocalStore.saveAnchor(newAnchor!, anchorkey: heartRateAnchor)

                let headers = [
                    "Authorization": "Basic " + self.auth
                ]
                
                for data in results {
                    
                    print(data.sourceRevision.source.name)
                    if data.sourceRevision.source.name == "Withings" {
                        
                        let heartRate = data as? HKQuantitySample
                        
                        //let date = String(mostRecentHeartRate.endDate)
                        let measurement = Float(heartRate!.quantity.doubleValueForUnit(HKUnit(fromString: "count/s"))*60)
                        let date = data.endDate as NSDate
                        let dateString = HelperFunctions.convertDateToString(date)
                        
                        let source = data.sourceRevision.source.name
                        
                        
                        let parameters: [String:AnyObject] = [
                            "measurement": measurement,
                            "date": dateString,
                            "username": self.username,
                            "source": source
                        ]
                        
                        // Post request to DTU Server
                        Alamofire.request(.POST, "http://heartux.compute.dtu.dk:443/heartrate", headers: headers, parameters: parameters)
                            .response {(request, response, data, error) in
                                if error != nil {
                                    print(error)
                                    completionHandler()

                                    
                                } else {
                                    let responsedata = JSON(data: data!)
                                    print(responsedata)
                                    completionHandler()

                                }
                        }
                    }
                }
//                completionHandler()

            }
            
        }
        
    }
    
    func weightChangedHandler(query: HKObserverQuery!, completionHandler: HKObserverQueryCompletionHandler!, error: NSError!) {
        
        // Here you need to call a function to query the weight change
        let weightAnchor = "weightAnchor"
        
        let sampleType = HKSampleType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyMass)
        checkForNewSamples(sampleType!, anchor: LocalStore.getAnchor(weightAnchor)) { (results, newAnchor, error, anchorValue) -> Void in
            if( error != nil )
            {
                print("Error reading height from HealthKit Store: \(error.localizedDescription)")
                return;
            }
            
            // Send if new data is in healthKit to DTU server and show notificaton
            if(newAnchor != nil && newAnchor != anchorValue) {

                if NSUserDefaults.standardUserDefaults().objectForKey("username") != nil && NSUserDefaults.standardUserDefaults().objectForKey("authKey") != nil {
                    self.username = LocalStore.getUsernameForUser()
                    self.auth = LocalStore.getAuthKey()
                } else {
                    completionHandler()
                    return
                }
                
                LocalStore.saveAnchor(newAnchor!, anchorkey: weightAnchor)

                let headers = [
                    "Authorization": "Basic " + self.auth
                ]
                
                let lengthOfResults = results.count
                var checkLengthOfIndex: Int = 1
                
                for data in results {
                    
                    let weight = data as? HKQuantitySample
                    
                    let measurement = Float(weight!.quantity.doubleValueForUnit(HKUnit.gramUnitWithMetricPrefix(.Kilo)))
                    let date = data.endDate as NSDate
                    let dateString = HelperFunctions.convertDateToString(date)
                    let source = data.sourceRevision.source.name
                    
                    
                    let parameters: [String:AnyObject] = [
                        "measurement": measurement,
                        "date": dateString,
                        "username": self.username,
                        "source": source
                    ]
                    
                    // Post request to DTU Server
                    Alamofire.request(.POST, "http://heartux.compute.dtu.dk:443/weight", headers: headers, parameters: parameters)
                        .response {(request, response, data, error) in
                            if error != nil {
                                //print(error)
                                
                                // Send the notification to the user
                                let notification = UILocalNotification()
                                notification.alertBody = "Cant connect to server - inform Henrik"
                                notification.alertAction = "Open"
                                notification.soundName = UILocalNotificationDefaultSoundName
                                UIApplication.sharedApplication().scheduleLocalNotification(notification)
                            
//                                completionHandler()

                                
                            } else {
                                let response = JSON(data: data!)
                                
                                if response["_status"] == "OK" {
                                    
                                    // Send the notification to the user
                                    let notification = UILocalNotification()
                                    notification.alertBody = "New weight measurement (\(weight!.quantity)) succusfully sent to server"
                                    notification.alertAction = "Open"
                                    notification.soundName = UILocalNotificationDefaultSoundName
                                    UIApplication.sharedApplication().scheduleLocalNotification(notification)
                                    print(response)
                                    
                                } else {
                                    
                                    // Send the notification to the user
                                    print(response)
                                    let notification = UILocalNotification()
                                    notification.alertBody = "Failed to sent weight measurement to server - try again!"
                                    notification.alertAction = "Open"
                                    notification.soundName = UILocalNotificationDefaultSoundName
                                    UIApplication.sharedApplication().scheduleLocalNotification(notification)
                                    
                                }
                                
                                completionHandler()

                            }
                            
//
//                            if lengthOfResults <= checkLengthOfIndex {
//                                completionHandler()
//                            }
//                            
//                            checkLengthOfIndex += 1
                            
                    }
                }
//            completionHandler()

            }
            
        }
        
    }
    
    func checkForNewSamples(sampleType:HKSampleType, anchor: HKQueryAnchor?, completion: (([HKSample]!, HKQueryAnchor!, NSError!, HKQueryAnchor!) -> Void)!) {
        
        var anchorValue: HKQueryAnchor?

        if(anchor != nil){
            anchorValue = anchor!
        } else {
            anchorValue = HKQueryAnchor(fromValue: Int(HKAnchoredObjectQueryNoAnchor))
        }
        
//        let past = NSDate.distantPast()
        
        let past = NSDate(timeIntervalSinceNow: -30*24*60*60)
        let now   = NSDate()
        let predicate = HKQuery.predicateForSamplesWithStartDate(past, endDate:now, options: .None)
        
        //let sampleType = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyMass)
        
        let query = HKAnchoredObjectQuery(type: sampleType, predicate: predicate, anchor: anchorValue, limit: Int(HKObjectQueryNoLimit)) { (HKAnchoredObjectQuery, results, deleted,
            newAnchor, error) -> Void in
            
            if error != nil {
                print(error)
                completion(nil, nil, error, nil)
                return
            }
            
            // Get the first sample
            let measurements = results! as? [HKQuantitySample]
            //print(results)
            
            if completion != nil {
                completion(measurements,newAnchor,nil,anchorValue)
            }
        }
        healthKitStore.executeQuery(query)
    }
    
    func checkForNewSamplesBloodPressure(sampleType:HKSampleType, anchor: HKQueryAnchor?, completion: (([HKCorrelation]!, HKQueryAnchor!, NSError!, HKQueryAnchor!) -> Void)!) {
        
        var anchorValue: HKQueryAnchor?
        
        if(anchor != nil){
            anchorValue = anchor!
        } else {
            anchorValue = HKQueryAnchor(fromValue: Int(HKAnchoredObjectQueryNoAnchor))
        }
        
//        let past = NSDate.distantPast()
        
        // Query all data that is not more than 2 weeks old
        
        let past = NSDate(timeIntervalSinceNow: -30*24*60*60)
        let now   = NSDate()
        let mostRecentPredicate = HKQuery.predicateForSamplesWithStartDate(past, endDate:now, options: .None)
        
        let query = HKAnchoredObjectQuery(type: sampleType, predicate: mostRecentPredicate, anchor: anchorValue, limit: Int(HKObjectQueryNoLimit)) { (HKAnchoredObjectQuery, results, deleted,
            newAnchor, error) -> Void in
            
            if error != nil {
                print(error)
                completion(nil, nil, error, nil)
                return
            }
            
            // Get the first sample
            //let bloodPressure = results!.first as? HKCorrelation
            
            // Get all samples
            let bloodPressure = results! as? [HKCorrelation]

            if completion != nil {
                completion(bloodPressure,newAnchor,nil,anchorValue)
            }
        }
        healthKitStore.executeQuery(query)
    }


    



}

