//
//  helperFunctions.swift
//  HeartUX
//
//  Created by Henrik Holm on 01/01/2016.
//  Copyright © 2016 Henrik Holm. All rights reserved.
//

import UIKit

struct HelperFunctions {
    
    // Date

    static func convertDateToString(date: NSDate) -> String {
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd-MM-yy HH:mm:ss"
        dateFormatter.timeZone = NSTimeZone.localTimeZone()
        let dateString = dateFormatter.stringFromDate(date)
        
        return dateString
    }
    
    static func converStringToDateBloodPressure(date: String) -> NSDate {
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd-MM-yy HH:mm:ss"
        dateFormatter.timeZone = NSTimeZone.localTimeZone()
        let dateNSDate = dateFormatter.dateFromString(date)
        
        return dateNSDate!
    }
    
    static func convertStringToDate(date: String) -> NSDate {
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd-MM-yy"
        dateFormatter.timeZone = NSTimeZone.localTimeZone()
        let dateNSDate = dateFormatter.dateFromString(date)
        
        return dateNSDate!
    }
    
    static func convertDateToPythonDateString(date: NSDate) -> String {
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "EEE, dd MMM yyyy HH:mm:ss"
        dateFormatter.timeZone = NSTimeZone(name: "GMT")
        let dateString = dateFormatter.stringFromDate(date)
        
        return dateString
    }

    
    static func daysBetweenDate(startDate: NSDate, endDate: NSDate) -> Int
    {
        let calendar = NSCalendar.currentCalendar()
        
        let components = calendar.components([.Day], fromDate: startDate, toDate: endDate, options: [])
        
        return components.day
    }
    
    
    // Basic Auth
    
    static func getBasicAuthHeaders(username: String, password: String) -> String {
        
        let plainString = "\(username):\(password)" as NSString
        let plainData = plainString.dataUsingEncoding(NSUTF8StringEncoding)
        let base64String = plainData?.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        //print(base64String!)
        
        return base64String!
    }
    
    
}