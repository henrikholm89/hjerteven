//
//  MasterViewController.swift
//  HKTutorial
//
//  Created by ernesto on 18/10/14.
//  Copyright (c) 2014 raywenderlich. All rights reserved.
//

import Foundation
import HealthKit
import UIKit
import Alamofire
import SwiftyJSON

class MeasurementViewController: UITableViewController {
    
    let healthManager:HealthManager = HealthManager()
    let dtuServer:DTUServer = DTUServer()

    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    // Weight labels
    @IBOutlet var statusTextWeight: UILabel!
    @IBOutlet var lastestWeightMeasurement: UILabel!
    @IBOutlet var daysAgoOfLatestWeightMeasurement: UILabel!
    
    // Blood Pressure labels
    @IBOutlet var statusTextBloodPressure: UILabel!
    @IBOutlet var daysAgoOfLastestBloodPressureMeasurement: UILabel!
    @IBOutlet var latestBloodPressureMeasurement: UILabel!
    
    //var measurement: [String] = []
    //var date: [String] = []
    var weights: [Weight] = []
    var bloodPressures: [BloodPressure] = []
    
    let username = NSUserDefaults.standardUserDefaults().objectForKey("username")! as! String
    let auth = NSUserDefaults.standardUserDefaults().objectForKey("authKey")! as! String
    
    
//    @IBOutlet weak var segmentedControl: UISegmentedControl!
//    
//    @IBAction func indexChanged(sender: AnyObject) {
//        
//        switch segmentedControl.selectedSegmentIndex {
//            
//        case 0:
//            dtuServer.getUserWeightsFromDTUServer(username, auth: auth) { (json, error) -> Void in
//                
//                if error != nil {
//                    print(error)
//                } else {
//                    self.measurement = []
//                    self.date = []
//                    
//                    var weightArray: [JSON] = []
//                    weightArray = json["_items"].arrayValue
//                    print(weightArray.count)
//                    
//                    for wa in weightArray {
//                        let weight = wa["measurement"].floatValue
//                        let weightFormatted = String(format: "%.2f", weight) + " kg"
//
//                        let date = wa["date"].stringValue
//                        
//                        self.measurement.append(weightFormatted)
//                        self.date.append(date)
//
//                    }
//                    self.tableView.reloadData()
//                }
//            }
//        case 1:
//            dtuServer.getUserBodyFatFromDTUServer(username, auth: auth) { (json, error) -> Void in
//                
//                if error != nil {
//                    print(error)
//                } else {
//                    self.measurement = []
//                    self.date = []
//                    
//                    var weightArray: [JSON] = []
//                    
//                    weightArray = json["_items"].arrayValue
//                    print(weightArray.count)
//                    
//                    for wa in weightArray {
//                        let weight = wa["measurement"].floatValue
//                        let weightFormatted = String(format: "%.2f", weight) + " %"
//                        
//                        let date = wa["date"].stringValue
//                        
//                        self.measurement.append(weightFormatted)
//                        self.date.append(date)
//                        
//
//                    }
//                    self.tableView.reloadData()
//                }
//            }
//        case 2:
//            dtuServer.getUserBloodPressureFromDTUServer(username, auth: auth) { (json, error) -> Void in
//                
//                if error != nil {
//                    print(error)
//                } else {
//                    self.measurement = []
//                    self.date = []
//                    
//                    var weightArray: [JSON] = []
//                    
//                    weightArray = json["_items"].arrayValue
//                    print(weightArray.count)
//                    
//                    for wa in weightArray {
//                        let systolic = wa["systolic"].floatValue
//                        let systolicFormatted = String(format: "%.0f", systolic)
//                        
//                        let diastolic = wa["diastolic"].floatValue
//                        let diastolicFormatted = String(format: "%.0f", diastolic)
//                        
//                        let bloodpressure: String = systolicFormatted + "/" + diastolicFormatted
//                        let date = wa["date"].stringValue
//                        
//                        self.measurement.append(bloodpressure)
//                        self.date.append(date)
//                        
//                        
//                    }
//                    self.tableView.reloadData()
//                }
//            }
//
//        default:
//            break
//        }
//    }
    
    func loadWeightOverviewTheLast3Days() {
        
        dtuServer.getUserWeightsGroupedFromDTUServer(username, auth: auth, last3days: true) { (weightsGroupedByDate, mostRecentWeight, error) -> Void in
            
            if error != nil {
                print(error)
            } else {
                
                for wgbd in weightsGroupedByDate {
                    print(wgbd.measurement)
                    print(wgbd.date)
                }
                print(weightsGroupedByDate.count)
                
                if weightsGroupedByDate.count > 1 {
                    
                    var measurementWithinLastThreeDays: Float = 0.0
                    
                    if weightsGroupedByDate.count > 3 {
                        measurementWithinLastThreeDays = weightsGroupedByDate[3].measurement
                    } else if weightsGroupedByDate.count > 2 {
                        measurementWithinLastThreeDays = weightsGroupedByDate[2].measurement
                    } else {
                        measurementWithinLastThreeDays = weightsGroupedByDate[1].measurement
                    }
                    
                    let mostRecentMeasurement = weightsGroupedByDate[0].measurement
                    
                    let weightDifference = mostRecentMeasurement - measurementWithinLastThreeDays
                    
                    print(weightDifference)
                    if weightDifference <= -2 {
                        self.statusTextWeight.text = "You have lost more than 2 kg the last 3 days - contact medical staff"
                    } else if weightDifference < -1 {
                        self.statusTextWeight.text = "You have lost more than 1 kg the last 3 days - keep an eye on it"
                    } else if weightDifference > 1 && weightDifference < 2 {
                        self.statusTextWeight.text = "You have gained more than 1 kg the last 3 days - keep an eye on it"
                    } else if weightDifference >= 2 {
                        self.statusTextWeight.text = "You have gained more than 2 kg the last 3 days - contact medical staff"
                    } else {
                        self.statusTextWeight.text = "Your weight has been stable the last 3 days"
                    }
                    
                } else if weightsGroupedByDate.count > 0 {
                    
                    self.statusTextWeight.text = "You have only weigh yourself once within the last 3 days - weigh yourself again"
                
                } else {
                    self.statusTextWeight.text = "You have not weigh yourself within the last 3 days. We recommend you do it"
                }
                
            }
        }
        
    }

    func loadWeightOverview() {
        
        dtuServer.getUserWeightsGroupedFromDTUServer(username, auth: auth, last3days: false) { (weightsGroupedByDate, mostRecentWeight, error) -> Void in
            
            if error != nil {
                print(error)
            } else {
                
                self.weights = weightsGroupedByDate
                
                if weightsGroupedByDate.count > 0 {
                    
                    let dateToday = NSDate()
                    let latestWeightMeasureDate = HelperFunctions.convertStringToDate(weightsGroupedByDate[0].date)
                    
                    if HelperFunctions.daysBetweenDate(latestWeightMeasureDate, endDate: dateToday) > 1 {
                        self.daysAgoOfLatestWeightMeasurement.text = String(HelperFunctions.daysBetweenDate(latestWeightMeasureDate, endDate: dateToday)) + " days ago"
                    } else {
                        self.daysAgoOfLatestWeightMeasurement.text = String(HelperFunctions.daysBetweenDate(latestWeightMeasureDate, endDate: dateToday)) + " day ago"
                    }
                    
                    let mostRecentWeightFormatted = String(format: "%.2f", mostRecentWeight) + " kg"
                    self.lastestWeightMeasurement.text = String(mostRecentWeightFormatted)
                    
                    
                } else {
                    self.daysAgoOfLatestWeightMeasurement.text = ""
                    self.lastestWeightMeasurement.text = ""
                }
                
            }
        }

        
    }
    
    func loadBloodPressure() {
        dtuServer.getUserBloodPressureFromDTUServer(username, auth: auth) { (bloodpressureArray, error) -> Void in
            if error != nil {
                print(error)
            } else {
                
                self.bloodPressures = bloodpressureArray
                
                if bloodpressureArray.count > 0 {
                    
                    let dateToday = NSDate()
                    let latestBloodPressureMeasureDate = HelperFunctions.converStringToDateBloodPressure(bloodpressureArray[0].date)
                    let daysSinceLastMeasurement = HelperFunctions.daysBetweenDate(latestBloodPressureMeasureDate, endDate: dateToday)
                    
                    let mostRecentBloodPressureMeasurement = bloodpressureArray[0].bloodPressureCombined
                    self.latestBloodPressureMeasurement.text = String(mostRecentBloodPressureMeasurement)
                    let mostRecentSystolicMeasurement = bloodpressureArray[0].systolic
                    
                    if daysSinceLastMeasurement > 3 {
                        self.daysAgoOfLastestBloodPressureMeasurement.text = String(HelperFunctions.daysBetweenDate(latestBloodPressureMeasureDate, endDate: dateToday)) + " days ago"
                        self.statusTextBloodPressure.text = "You have not measured your blood pressure within the last 3 days. We recommend you take one."
                        
                    } else if daysSinceLastMeasurement > 1 && daysSinceLastMeasurement <= 3 {
                        self.daysAgoOfLastestBloodPressureMeasurement.text = String(HelperFunctions.daysBetweenDate(latestBloodPressureMeasureDate, endDate: dateToday)) + " days ago"
                        
                        if mostRecentSystolicMeasurement > 130 {
                            self.statusTextBloodPressure.text = "Your systolic blood pressure is too high - contact medical staff"
                        } else if mostRecentSystolicMeasurement < 95 {
                            self.statusTextBloodPressure.text = "Your systolic blood pressure is too low - contact medical staff"
                        } else {
                            self.statusTextBloodPressure.text = "Your blood pressure is within normal range :)"
                        }

                    } else {
                        self.daysAgoOfLastestBloodPressureMeasurement.text = String(HelperFunctions.daysBetweenDate(latestBloodPressureMeasureDate, endDate: dateToday)) + " day ago"
                        
                        if mostRecentSystolicMeasurement > 130 {
                            self.statusTextBloodPressure.text = "Your systolic blood pressure is too high - contact medical staff"
                        } else if mostRecentSystolicMeasurement < 95 {
                            self.statusTextBloodPressure.text = "Your systolic blood pressure is too low - contact medical staff"
                        } else {
                            self.statusTextBloodPressure.text = "Your blood pressure is within normal range :)"
                        }

                    }
                    
                } else {
                    
                    self.daysAgoOfLastestBloodPressureMeasurement.text = ""
                    self.latestBloodPressureMeasurement.text = ""
                    self.statusTextBloodPressure.text = "You have not taken any blood pressure measurements yet. We recommend you take one."
                }
                
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //loadWeightOverview()
        

    }

    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        loadWeightOverview()
        loadWeightOverviewTheLast3Days()
        loadBloodPressure()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showWeights" {
            print("showWeight")
            
            let object = self.weights
            
            let vc = segue.destinationViewController as! WeightTableViewController
            vc.weights = object

           
        }
        if segue.identifier == "showBloodPressure" {
            print("showBloodPressure")
            
            let object = self.bloodPressures
            
            let vc = segue.destinationViewController as! BloodPressureTableViewController
            vc.bloodpressures = object
            
 
        }
    }

}


