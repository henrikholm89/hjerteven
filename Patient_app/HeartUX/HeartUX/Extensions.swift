//
//  File.swift
//  HeartUX
//
//  Created by Henrik Holm on 02/01/2016.
//  Copyright © 2016 Henrik Holm. All rights reserved.
//

import Foundation
import UIKit

// Date extensions:

extension NSDate {
    var formatted:String {
        let formatter = NSDateFormatter()
        formatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
        return formatter.stringFromDate(self)
    }
    func formattedWith(format:String) -> String {
        let formatter = NSDateFormatter()
        formatter.dateFormat = format
        return formatter.stringFromDate(self)
    }
}

extension String {
    var asDate:NSDate! {
        let styler = NSDateFormatter()
        styler.dateFormat = "dd-MM-yyyy HH:mm:ss"
        return styler.dateFromString(self)!
    }
    func asDateFormattedWith(format:String) -> NSDate! {
        let styler = NSDateFormatter()
        styler.dateFormat = format
        return styler.dateFromString(self)!
    }
    
}

// String extensions:


extension String
{
    func substringWithRange(start: Int, end: Int) -> String
    {
        if (start < 0 || start > self.characters.count)
        {
            print("start index \(start) out of bounds")
            return ""
        }
        else if end < 0 || end > self.characters.count
        {
            print("end index \(end) out of bounds")
            return ""
        }
        let range = Range(start: self.startIndex.advancedBy(start), end: self.startIndex.advancedBy(end))
        return self.substringWithRange(range)
    }
    
    func substringWithRange(start: Int, location: Int) -> String
    {
        if (start < 0 || start > self.characters.count)
        {
            print("start index \(start) out of bounds")
            return ""
        }
        else if location < 0 || start + location > self.characters.count
        {
            print("end index \(start + location) out of bounds")
            return ""
        }
        let range = Range(start: self.startIndex.advancedBy(start), end: self.startIndex.advancedBy(start + location))
        return self.substringWithRange(range)
    }
}

// Array extensions:

extension Array where Element: Equatable {
    mutating func removeObject(object: Element) {
        if let index = self.indexOf(object) {
            self.removeAtIndex(index)
        }
    }
    
    mutating func removeObjectsInArray(array: [Element]) {
        for object in array {
            self.removeObject(object)
        }
    }
}