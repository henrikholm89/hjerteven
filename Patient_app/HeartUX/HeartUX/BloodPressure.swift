//
//  BloodPressure.swift
//  HeartUX
//
//  Created by Henrik Holm on 07/01/2016.
//  Copyright © 2016 Henrik Holm. All rights reserved.
//

import UIKit

class BloodPressure: NSObject {

    var username: String = ""
    var systolic: Float = 0.0
    var diastolic: Float = 0.0
    var bloodPressureCombined: String = ""
    var date: String = ""
    var sourceType : String = ""
    
}
