//
//  Weight.swift
//  HeartUX
//
//  Created by Henrik Holm on 20/12/2015.
//  Copyright © 2015 Henrik Holm. All rights reserved.
//

import UIKit

class Weight: NSObject {
    
    var username : String = ""
    var measurement : Float = 0.0
    var date: String = ""
    var sourceType : String = ""
    
}
