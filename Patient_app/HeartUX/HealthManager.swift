//
//  HealthManager.swift
//  HKTutorial
//
//  Created by ernesto on 18/10/14.
//  Copyright (c) 2014 raywenderlich. All rights reserved.
//

import Foundation
import HealthKit
import UIKit

class HealthManager: UITableViewController  {
  
  let healthKitStore:HKHealthStore = HKHealthStore()
  var rtVC = RealTimeViewController()
  
  // Authorize HealthKit //
  func authorizeHealthKit(completion: ((success:Bool, error:NSError!) -> Void)!)
  {
    // 1. Set the types you want to read from HK Store
    let healthKitTypesToRead = Set(
      arrayLiteral: HKObjectType.characteristicTypeForIdentifier(HKCharacteristicTypeIdentifierDateOfBirth)!,
      HKObjectType.characteristicTypeForIdentifier(HKCharacteristicTypeIdentifierBloodType)!,
      HKObjectType.characteristicTypeForIdentifier(HKCharacteristicTypeIdentifierBiologicalSex)!,
      HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyMass)!,
      HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeight)!,
      HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierOxygenSaturation)!,
      HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyFatPercentage)!,
      HKObjectType.workoutType()
    )
    
    // 2. Set the types you want to write to HK Store
    let healthKitTypesToWrite = Set(
      arrayLiteral: HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyMassIndex)!,
      HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierActiveEnergyBurned)!,
      HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDistanceWalkingRunning)!,
      HKQuantityType.workoutType()
    )
    
    // 3. If the store is not available (for instance, iPad) return an error and don't go on.
    if !HKHealthStore.isHealthDataAvailable()
    {
      let error = NSError(domain: "com.raywenderlich.tutorials.healthkit", code: 2, userInfo: [NSLocalizedDescriptionKey:"HealthKit is not available in this Device"])
      if( completion != nil )
      {
        completion(success:false, error:error)
      }
      return;
    }
    
    // 4.  Request HealthKit authorization
    healthKitStore.requestAuthorizationToShareTypes(healthKitTypesToWrite, readTypes: healthKitTypesToRead) { (success, error) -> Void in
      
      if( completion != nil )
      {
        dispatch_async(dispatch_get_main_queue(), self.startObservingWeightChanges)
        completion(success:success,error:error)
      }
    }
  }
  
  func startObservingWeightChanges() {
    
    let sampleType = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyMass)
    
    let query = HKObserverQuery(sampleType: sampleType!, predicate: nil) {
      query, completionHandler, error in
      
      if error != nil {
        print(error)
      } else {
        self.weightChangedHandler(query, completionHandler: completionHandler, error: error)
        //self.rtVC.updateWeight()
        //self.rtVC.tableView.reloadData()
        //completionHandler()
      }
    }
    
    healthKitStore.executeQuery(query)
    healthKitStore.enableBackgroundDeliveryForType(sampleType!, frequency: .Immediate, withCompletion: {(succeeded, error) in
      
      if succeeded{
        print("Enabled background delivery of weight changes")
      } else {
        if let theError = error{
          print("Failed to enable background delivery of weight changes. ")
          print("Error = \(theError)")
        }
      }
    })
  }
  
  func weightChangedHandler(query: HKObserverQuery!, completionHandler: HKObserverQueryCompletionHandler!, error: NSError!) {
    
    // Here you need to call a function to query the height change
    rtVC.updateWeight()
  
    // Send the notification to the user
    let notification = UILocalNotification()
    notification.alertBody = "New weight measurement in Health App"
    notification.alertAction = "open"
    notification.soundName = UILocalNotificationDefaultSoundName
    
    UIApplication.sharedApplication().scheduleLocalNotification(notification)
    
    completionHandler()
  }
  

  
  // Read Profile //
  func readProfile() -> ( age:Int?,  biologicalsex:HKBiologicalSexObject?, bloodtype:HKBloodTypeObject?)
  {
    var error:NSError?
    var age:Int?
    
    // 1. Request birthday and calculate age
    do {
      let birthDay = try healthKitStore.dateOfBirth()
      let today = NSDate()
      let calendar = NSCalendar.currentCalendar()
      let differenceComponents = NSCalendar.currentCalendar().components(.Year, fromDate: birthDay, toDate: today, options: NSCalendarOptions(rawValue: 0) )
      age = differenceComponents.year
    } catch var error1 as NSError {
      error = error1
    }
    if error != nil {
      print("Error reading Birthday: \(error)")
    }
    
    // 2. Read biological sex
    var biologicalSex:HKBiologicalSexObject?
    do {
      biologicalSex = try healthKitStore.biologicalSex()
    } catch var error1 as NSError {
      error = error1
      biologicalSex = nil
    };
    if error != nil {
      print("Error reading Biological Sex: \(error)")
    }
    // 3. Read blood type
    var bloodType:HKBloodTypeObject?
    do {
      bloodType = try healthKitStore.bloodType()
    } catch var error1 as NSError {
      error = error1
      bloodType = nil
    };
    if error != nil {
      print("Error reading Blood Type: \(error)")
    }
    
    // 4. Return the information read in a tuple
    return (age, biologicalSex, bloodType)
  }
  
  // Read Latest Sample from HealthKit //
  func readMostRecentSample(sampleType:HKSampleType , completion: ((HKSample!, NSError!) -> Void)!)
  {
    
    // 1. Build the Predicate
    let past = NSDate.distantPast() 
    let now   = NSDate()
    let mostRecentPredicate = HKQuery.predicateForSamplesWithStartDate(past, endDate:now, options: .None)
    
    // 2. Build the sort descriptor to return the samples in descending order
    let sortDescriptor = NSSortDescriptor(key:HKSampleSortIdentifierStartDate, ascending: false)
    // 3. we want to limit the number of samples returned by the query to just 1 (the most recent)
    let limit = 1
    
    // 4. Build samples query
    let sampleQuery = HKSampleQuery(sampleType: sampleType, predicate: mostRecentPredicate, limit: limit, sortDescriptors: [sortDescriptor])
      { (sampleQuery, results, error ) -> Void in
        
        if let queryError = error {
          completion(nil,error)
          return;
        }
        
        // Get the first sample
        let mostRecentSample = results!.first as? HKQuantitySample
        
        // Execute the completion closure
        if completion != nil {
          completion(mostRecentSample,nil)
        }
    }
    // 5. Execute the Query
    self.healthKitStore.executeQuery(sampleQuery)
  }
  

  
  func saveBMISample(bmi:Double, date:NSDate ) {
    
    // 1. Create a BMI Sample
    let bmiType = HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyMassIndex)
    let bmiQuantity = HKQuantity(unit: HKUnit.countUnit(), doubleValue: bmi)
    let bmiSample = HKQuantitySample(type: bmiType!, quantity: bmiQuantity, startDate: date, endDate: date)
    
    // 2. Save the sample in the store
    healthKitStore.saveObject(bmiSample, withCompletion: { (success, error) -> Void in
      if( error != nil ) {
        print("Error saving BMI sample: \(error!.localizedDescription)")
      } else {
        print("BMI sample saved successfully!")
      }
    })
  }
  
  func saveRunningWorkout(startDate:NSDate , endDate:NSDate , distance:Double, distanceUnit:HKUnit , kiloCalories:Double,
    completion: ( (Bool, NSError!) -> Void)!) {
      
      // 1. Create quantities for the distance and energy burned
      let distanceQuantity = HKQuantity(unit: distanceUnit, doubleValue: distance)
      let caloriesQuantity = HKQuantity(unit: HKUnit.kilocalorieUnit(), doubleValue: kiloCalories)
      
      // 2. Save Running Workout
      let workout = HKWorkout(activityType: HKWorkoutActivityType.Running, startDate: startDate, endDate: endDate, duration: abs(endDate.timeIntervalSinceDate(startDate)), totalEnergyBurned: caloriesQuantity, totalDistance: distanceQuantity, metadata: nil)
      healthKitStore.saveObject(workout, withCompletion: { (success, error) -> Void in
        if( error != nil  ) {
          // Error saving the workout
          completion(success,error)
        }
        else {
          // if success, then save the associated samples so that they appear in the Health Store
          let distanceSample = HKQuantitySample(type: HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDistanceWalkingRunning)!, quantity: distanceQuantity, startDate: startDate, endDate: endDate)
          let caloriesSample = HKQuantitySample(type: HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierActiveEnergyBurned)!, quantity: caloriesQuantity, startDate: startDate, endDate: endDate)
          
          self.healthKitStore.addSamples([distanceSample,caloriesSample], toWorkout: workout, completion: { (success, error ) -> Void in
            completion(success, error)
          })
          
        }
      })
  }
  

  func readRunningWorkOuts(completion: (([AnyObject]!, NSError!) -> Void)!) {
    
    // 1. Predicate to read only running workouts
    let predicate =  HKQuery.predicateForWorkoutsWithWorkoutActivityType(HKWorkoutActivityType.Running)
    // 2. Order the workouts by date
    let sortDescriptor = NSSortDescriptor(key:HKSampleSortIdentifierStartDate, ascending: false)
    // 3. Create the query
    let sampleQuery = HKSampleQuery(sampleType: HKWorkoutType.workoutType(), predicate: predicate, limit: 0, sortDescriptors: [sortDescriptor])
      { (sampleQuery, results, error ) -> Void in
        
        if let queryError = error {
          print( "There was an error while reading the samples: \(queryError.localizedDescription)")
        }
        completion(results,error)
    }
    // 4. Execute the query
    healthKitStore.executeQuery(sampleQuery)
    
  }

  
  
}