RESOURCE_METHODS = ['GET','POST','DELETE']

ITEM_METHODS = ['GET','PATCH','DELETE']


X_DOMAINS = '*'
X_HEADERS = ['Authorization','If-Match','Access-Control-Expose-Headers','Content-Type','Pragma','Cache-Control']
X_EXPOSE_HEADERS = ['Origin', 'X-Requested-With', 'Content-Type', 'Accept']
DATE_FORMAT = "%d-%m-%y %H:%M:%S"

DOMAIN = {
    'user': {
        'additional_lookup': {
            'url': 'regex("[\w]+")',
            'field': 'username',
            },
        'schema': {
            'firstname': {
                'type': 'string'
            },
            'lastname': {
                'type': 'string'
            },
            'username': {
                'type': 'string',
                'unique': True
            },
            'password': {
                'type': 'string'
            },
            'cprnr': {
                'type': 'string'
            }
        }
    },
    'weight': {
        'additional_lookup': {
            'url': 'regex("[\w]+")',
            'field': 'username',
            },
        'schema': {
            'measurement': {
                'type': 'float'
            },
            'date': {
                'type': 'datetime'
            },
            'source': {
                'type': 'string'
            },
            'username': {
                'type': 'string'
            }
        }
    },
    'bodyfat': {
        'schema': {
            'measurement': {
                'type': 'float'
            },
            'date': {
                'type': 'datetime'
            },
            'source': {
                'type': 'string'
            },
            'username': {
                'type': 'string'
            }
        }
    },
    'heartrate': {
        'type': 'list',
            'schema': {
                'heartrate': {
                    'type': 'dict',
                    'schema': {
                        'meausurement': {'type': 'float'},
                        'date': {'type': 'datetime'},
                        'soruce': {'type': 'string'}
                    },
                },
            }
    },
    'bloodpressure': {
        'schema': {
            'systolic': {
                'type': 'float'
            },
            'diastolic': {
                'type': 'float'
            },
            'date': {
                'type': 'datetime'
            },
            'source': {
                'type': 'string'
            },
            'username': {
                'type': 'string'
            }
        }
    }
}
