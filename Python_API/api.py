# -*- coding: utf-8-sig -*-
from eve import Eve
from eve.auth import BasicAuth
import logging

class Authenticate(BasicAuth):
    def check_auth(self, username, password, allowed_roles, resource, method):
        if resource == 'user' and method == 'GET':
            user = app.data.driver.db['user']
            user = user.find_one({'username': username,'password':password})
            if user:
                return True
            else:
                return False
        elif resource == 'user' and method == 'POST':
            return username == 'admin' and password == 'kummefryser'
        elif resource == 'weight' and method == 'GET':
            user = app.data.driver.db['user']
            user = user.find_one({'username': username,'password':password})
            if user:
                return True
            else:
                return False
        elif resource in ['weight','bodyfat','heartrate','bloodpressure'] and method == 'POST':
            user = app.data.driver.db['user']
            user = user.find_one({'username': username,'password':password})
            if user:
                return True
            else:
                return False
        else:
            return True

def log_every_get(resource, request, payload):
    # custom INFO-level message is sent to the log file
    app.logger.info('We just answered to a POST request!')


port = 5000
host = '0.0.0.0'

if __name__ == '__main__':
    app = Eve(auth=Authenticate)
    app.run(host=host,port=port)
    #app.run()
