### Clone the Repository and Navigate to the project directory
```
git clone https://bitbucket.org/henrikholm89/hjerteven.git
cd hjerteven
```
## WEB APP ##

You need to have npm and bower installed on your system

### Install Dependencies 
```
npm install 
bower install
```
### Start the node.js server
```
node app.js
```

### Point your browser to `http://localhost:3000` and you should have the app running ###

## iOS Patient/HeartUX App ##

go to /Patient_app/HeartUX

### Install Dependencies 

```
pod install
```