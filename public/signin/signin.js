'use strict';

angular.module('signin', ['base64','ngRoute','myAppService', 'ngCookies'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/signin', {
    templateUrl: '../signin/signin.html',
    controller: 'SignInCtrl'
  });
}])


.controller('SignInCtrl',['$scope','$http','$base64','$window','$location','$cookies','CommonProp',function($scope,$http,$base64,$window,$location,$cookies,CommonProp){

$scope.signIn = function(){
	var username = $scope.username;
	var password = $scope.password;
  var authdata = $base64.encode(username + ':' + password);

  var today = new Date();
  var expired = new Date(today);
  expired.setDate(today.getDate() + 1); //Set expired date to tomorrow

	$http.defaults.headers.common = {"Access-Control-Request-Headers": "accept, origin, authorization"};
  $http.defaults.headers.common = {"Access-Control-Expose-Headers": "Origin, X-Requested-With, Content-Type, Accept"};
	$http.defaults.headers.common["Cache-Control"] = "no-cache";
	$http.defaults.headers.common.Pragma = "no-cache";
	$http.defaults.headers.common['Authorization'] = 'Basic '+authdata;

	$http({method: 'GET',cache: false, url: 'http://heartux.compute.dtu.dk:443/user/'+ username}).
            success(function(data, status, headers, config) {
          		CommonProp.setUser(username);
          		CommonProp.setUserAuth(authdata);
              $cookies.put('user', username, {expires : expired });
              $cookies.put('authdata', authdata, {expires : expired });
              console.log("logged in")
		          $location.path('/userHome');
            }).
            error(function(data, status, headers, config) {
                console("failed at login")
                console.log(data,status);
            });

};


}]);
