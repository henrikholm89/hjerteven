'use strict';

angular.module('userHome', ['ngRoute','myAppService', 'chart.js', 'ngCookies'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/userHome', {
    templateUrl: '../userHome/userHome.html',
    controller: 'UserHomeCtrl'
  });
}])

.controller('UserHomeCtrl',['$scope','CommonProp','$http','$location','$cookies', function($scope,CommonProp,$http,$location,$cookies) {

	//var auth = CommonProp.getUserAuth();
  //var user = CommonProp.getUser();

  var user = $cookies.get("user");
  var auth = $cookies.get("authdata");
  var url = ""

  if (user == "admin") {
    url = 'http://heartux.compute.dtu.dk:443/user'
  } else {
    url = 'http://heartux.compute.dtu.dk:443/user/' + user
  }

	var getAllUsers = function(){
      $scope.users = [];
      $http.defaults.headers.common = {"Access-Control-Request-Headers": "accept, origin, authorization"};
      $http.defaults.headers.common = {"Access-Control-Expose-Headers": "Origin, X-Requested-With, Content-Type, Accept"};
    	$http.defaults.headers.common["Cache-Control"] = "no-cache";
      $http.defaults.headers.common.Pragma = "no-cache";
      $http.defaults.headers.common['Authorization'] = 'Basic '+auth;

    	$http({method: 'GET',cache: false, url: url}).
    	    success(function(data, status, headers, config) {
          	if (user == "admin") {
              for(var i=0;i<data._items.length;i++){
                  if (data._items[i].username != "admin") {
                    console.log(data._items[i].firstname);
                    $scope.users.push({'username': data._items[i].username, 'firstname': data._items[i].firstname, 'lastname': data._items[i].lastname, 'cprnr': data._items[i].cprnr});
                  }
              }
            } else {
              $scope.users.push({'username': data.username, 'firstname': data.firstname, 'lastname': data.lastname});
            }

    	    }).
    	    error(function(data, status, headers, config) {
    	        console.log(data,status);
    	    });
  }

	getAllUsers();

  $scope.getWeights = function(username, firstname, lastname, cprnr){ //and CPR
        //alert("Username is "+username);
        $scope.username = username;
        $scope.firstname = firstname;
        $scope.lastname = lastname;
        $scope.cprnr = cprnr;
        //shoud also include $scope.cpr = cpr;
        $scope.weights = [];
        $scope.datesW = [];
        $scope.datesRandom = [];
        $scope.series = [];
        $scope.weightArray = [];

        //Random generation of Heart Rate is found here:
        $scope.heartRateArray = [];
        $scope.heartRates = [];
        $scope.heartRateArrayMax = [];
        $scope.heartRatesMax = [];
        $scope.seriesHeartRate = [];
        //Random steps
        $scope.stepArray = [];
        $scope.step = [];
        //Random activity
        $scope.activitySerie = [];
        $scope.activity = [];

    		$http.defaults.headers.common = {"Access-Control-Request-Headers": "accept, origin, authorization"};
    	  $http.defaults.headers.common = {"Access-Control-Expose-Headers": "Origin, X-Requested-With, Content-Type, Accept"};
    		$http.defaults.headers.common["Cache-Control"] = "no-cache";
      	$http.defaults.headers.common.Pragma = "no-cache";
      	$http.defaults.headers.common['Authorization'] = 'Basic '+auth;

        $http({method: 'GET', cache: false, url: 'http://heartux.compute.dtu.dk:443/weight?where={"username":"' + username + '"}&sort=-date'}).
    		    success(function(data, status, headers, config) {

              var min = 70;
              var max = 90;
              var min1 = min + 20;
              var max1 = max + 20;
              var stepMax = 12500;
              var stepMin = 3000;
              var weightObjectArray = [];
              var dates = [];

              for(var i=0;i<data._items.length;i++) {
                  var weightObject = {};
                  weightObject.weight = data._items[i].measurement
                  weightObject.date = data._items[i].date.slice(0,5)
                  weightObjectArray.push(weightObject)

                  $scope.datesRandom.push(data._items[i].date.slice(0,5))
                  $scope.heartRates.push(Math.floor(Math.random() * (max - min)+min));
                  $scope.heartRatesMax.push(Math.floor(Math.random() * (max1 - min1)+min1));
                  $scope.step.push(Math.floor(Math.random() * (stepMax - stepMin)+stepMin));
                  }

              var groupOnDates = alasql('SELECT date, AVG(CAST([weight] AS FLOAT)) AS meanWeight \ FROM ? GROUP BY date', [weightObjectArray]);

              for (var j=0; j<groupOnDates.length;j++) {
                console.log(groupOnDates[j].date)
                $scope.weights.push(parseFloat(groupOnDates[j].meanWeight.toFixed(2)));
                dates.push(groupOnDates[j].date);
              }

              $scope.activitySerie = ["Inaktiv", "Gang", "Løb"]
              $scope.series = ["Min. puls", "Max. puls"]
              $scope.heartRateArray.push($scope.heartRates);
              $scope.heartRateArrayMax.push($scope.heartRatesMax);
              $scope.seriesHeartRate.push($scope.heartRates, $scope.heartRatesMax);
              $scope.stepArray.push($scope.step);
              $scope.activity.push(80, 17, 3);

              $scope.weightArray.push($scope.weights.reverse())
              $scope.datesW = dates.reverse()
    		    }).
    		    error(function(data, status, headers, config) {
    		        console.log(data,status);
    		    });
            $scope.onClick = function (points, evt) {
              console.log(points, evt);
            };

  	};

    $scope.getBloodPressure = function(username){ //and CPR

          $scope.systolic = [];
          $scope.diastolic = [];
          $scope.datesBP = [];
          $scope.seriesBP = [];
          $scope.bloodpressureArray = [];

          $http.defaults.headers.common = {"Access-Control-Request-Headers": "accept, origin, authorization"};
          $http.defaults.headers.common = {"Access-Control-Expose-Headers": "Origin, X-Requested-With, Content-Type, Accept"};
          $http.defaults.headers.common["Cache-Control"] = "no-cache";
          $http.defaults.headers.common.Pragma = "no-cache";
          $http.defaults.headers.common['Authorization'] = 'Basic '+auth;

          // $http({method: 'GET', cache: false, url: 'http://heartux.compute.dtu.dk:443/weight?where={"username":"testbruger"}'}).
          $http({method: 'GET', cache: false, url: 'http://heartux.compute.dtu.dk:443/bloodpressure?where={"username":"' + username + '"}&sort=-date'}).
              success(function(data, status, headers, config) {

                var bloodPressureObjectArray = [];
                var dates = [];

                for(var i=0;i<data._items.length;i++) {
                    var bloodPressureObject = {};
                    bloodPressureObject.systolic = parseInt(data._items[i].systolic)
                    bloodPressureObject.diastolic = parseInt(data._items[i].diastolic)
                    bloodPressureObject.date = data._items[i].date.slice(0,5)
                    bloodPressureObjectArray.push(bloodPressureObject)
                    }

                var groupOnDates = alasql('SELECT date, AVG(CAST([systolic] AS INT)) AS meanSystolic, AVG(CAST([diastolic] AS INT)) AS meanDiastolic \ FROM ? GROUP BY date', [bloodPressureObjectArray]);

                for (var j=0; j<groupOnDates.length;j++) {
                  $scope.systolic.push(groupOnDates[j].meanSystolic);
                  $scope.diastolic.push(groupOnDates[j].meanDiastolic);
                  dates.push(groupOnDates[j].date);
                }

              console.log($scope.datesBP)
              $scope.bloodpressureArray.push($scope.systolic.reverse(), $scope.diastolic.reverse())
              $scope.datesBP = dates.reverse()
              //console.log($scope.bloodpressureArray)
              $scope.seriesBP = ["Systolic", "Diastolic"]
              }).
              error(function(data, status, headers, config) {
                  console.log(data,status);
              });
              $scope.onClick = function (points, evt) {
                console.log(points, evt);
              };

      };

      $scope.getBodyFat = function(username){ //and CPR

            $scope.bodyfat = [];
            $scope.datesBF = [];
            $scope.bodyfatArray = [];

            $http.defaults.headers.common = {"Access-Control-Request-Headers": "accept, origin, authorization"};
            $http.defaults.headers.common = {"Access-Control-Expose-Headers": "Origin, X-Requested-With, Content-Type, Accept"};
            $http.defaults.headers.common["Cache-Control"] = "no-cache";
            $http.defaults.headers.common.Pragma = "no-cache";
            $http.defaults.headers.common['Authorization'] = 'Basic '+auth;

            // $http({method: 'GET', cache: false, url: 'http://heartux.compute.dtu.dk:443/weight?where={"username":"testbruger"}'}).
            $http({method: 'GET', cache: false, url: 'http://heartux.compute.dtu.dk:443/bodyfat?where={"username":"' + username + '"}&sort=-date'}).
                success(function(data, status, headers, config) {

                  var bodyFatObjectArray = [];
                  var dates = [];

                  for(var i=0;i<data._items.length;i++) {
                      var bodyFatObject = {};
                      bodyFatObject.bodyFat = data._items[i].measurement
                      bodyFatObject.date = data._items[i].date.slice(0,5)
                      bodyFatObjectArray.push(bodyFatObject)
                      }

                  var groupOnDates = alasql('SELECT date, AVG(CAST([bodyFat] AS FLOAT)) AS meanBodyFat \ FROM ? GROUP BY date', [bodyFatObjectArray]);

                  for (var j=0; j<groupOnDates.length;j++) {
                    console.log(groupOnDates[j].date)
                    $scope.bodyfat.push(parseFloat(groupOnDates[j].meanBodyFat.toFixed(2)));
                    dates.push(groupOnDates[j].date);
                  }

                $scope.bodyfatArray.push($scope.bodyfat.reverse());
                $scope.datesBF = dates.reverse()

                console.log($scope.bloodpressureArray)
                }).
                error(function(data, status, headers, config) {
                    console.log(data,status);
                });
                $scope.onClick = function (points, evt) {
                  console.log(points, evt);
                };

        };

}]);

//colours: ['#FF5252', '#FF8A80'],

function myFunction()
{
    document.getElementById('foo').setAttribute("class", "style1");
}

angular.module('organHome', [])

  .controller('inModalSwitchCtrl', function ($scope) {

  });
