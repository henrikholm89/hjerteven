angular.module('myApp', [
  'ngRoute',
  'home',
  'signin',
  'userHome',
  'addItem',
  'myAppService',
  'chart.js',
  'ngCookies'
]).
config(['$routeProvider', function($routeProvider) {
  $routeProvider.otherwise({redirectTo: '/home'});
  //console.log(angular.version)
  //console.log(Chart.version)
}]);
