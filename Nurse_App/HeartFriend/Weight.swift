//
//  WeightData.swift
//  HeartFriend
//
//  Created by Nima Zandi on 02/12/15.
//  Copyright © 2015 Nima Zandi. All rights reserved.
//

import UIKit

class Weight: NSObject
{
    
    var username : String = ""
    var measurement : Float = 0.0
    var date = NSDate()
    var sourceType : String = ""
   
}
