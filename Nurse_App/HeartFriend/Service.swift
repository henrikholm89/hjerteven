//
//  Service.swift
//  HeartFriend
//
//  Created by Nima Zandi on 01/12/15.
//  Copyright © 2015 Nima Zandi. All rights reserved.
//

import UIKit

class Service: NSObject
{
    let userPasswordString = "admin:kummefryser"
    
    var users = [UserObj]()
    var weights = [Weight]()
    
    
    func fetchUsers(callback:([UserObj])->Void) -> Void
    {
        let url = NSURL(string: "http://heartux.compute.dtu.dk:443/user")
        let config = NSURLSessionConfiguration.defaultSessionConfiguration()
        let userPasswordData = userPasswordString.dataUsingEncoding(NSUTF8StringEncoding)
        let base64EncodedCredential = userPasswordData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.Encoding64CharacterLineLength)
        let authString = "Basic \(base64EncodedCredential)"
        config.HTTPAdditionalHeaders = ["Authorization" : authString]
        let session = NSURLSession(configuration: config)
        
        var running = false
        
        
        
        let task = session.dataTaskWithURL(url!) {
            (let data, let response, let error) in
            if let httpResponse = response as? NSHTTPURLResponse
            {
                let dataString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print(dataString)
                
                
                do {
                    let json = try NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments)
                    
                    if let users = json["_items"] as? [[String: AnyObject]] {
                        for user in users {
                            if let username = user["username"] as? String {
                                if let firstname = user["firstname"] as? String {
                                    if let lastname = user["lastname"] as? String {
                                        
                                        let newUser = UserObj()
                                        newUser.username = username
                                        newUser.firstName = firstname
                                        newUser.lastname = lastname
                                        self.users.append(newUser)
                                    }
                                }
                                
                            }
                        }
                    }
                } catch {
                    print("error serializing JSON: \(error)")
                }
                
                
                // Finished
                callback(self.users)
                
            }
            running = false
        }
        
        running = true
        task.resume()
        
    }
    
    
    
    func fetchWeights(userName : String,  callback:([Weight])->Void) -> Void
    {
        
        let query1 = "http://heartux.compute.dtu.dk:443/weight?where=username=="
        let query2 = userName
        
        // Build the string with parameters
        let url = NSURL(string: query1 + query2)
        
        
        let config = NSURLSessionConfiguration.defaultSessionConfiguration()
        let userPasswordData = userPasswordString.dataUsingEncoding(NSUTF8StringEncoding)
        let base64EncodedCredential = userPasswordData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.Encoding64CharacterLineLength)
        let authString = "Basic \(base64EncodedCredential)"
        config.HTTPAdditionalHeaders = [    "Authorization" : authString]
        let session = NSURLSession(configuration: config)
        
        var running = false
        
        
        let task = session.dataTaskWithURL(url!) {
            (let data, let response, let error) in
            if let httpResponse = response as? NSHTTPURLResponse
            {
                let dataString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print(dataString)
                
                
                do {
                    let json = try NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments)
                    
                    if let users = json["_items"] as? [[String: AnyObject]] {
                        for user in users {
                            if let username = user["username"] as? String {
                                if let source = user["source"] as? String {
                                    if let measurement = user["measurement"] as? String {
                                        if let date = user["date"] as? String {
                                            
                                            
                                            var weightData = Weight()
                                            weightData.username = userName
                                            weightData.measurement = (measurement as NSString).floatValue
                                            weightData.sourceType = source
                                            
                                            
                                            var dateString = date
                                            let dateFormatter = NSDateFormatter()
                                            dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
                                            dateFormatter.timeZone = NSTimeZone(name: "UTC")
                                            let date = dateFormatter.dateFromString(dateString)
                                            
                                            
                                            
                                            weightData.date = date!
                                            
                                            self.weights.append(weightData)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch {
                    print("error serializing JSON: \(error)")
                }
                
                
                // Finished
                callback(self.weights)
                
            }
            running = false
        }
        
        running = true
        task.resume()
        
    }
    
    
    
    
    
}
